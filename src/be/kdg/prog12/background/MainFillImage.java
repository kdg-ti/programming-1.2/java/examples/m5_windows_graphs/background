package be.kdg.prog12.background;

import be.kdg.prog12.background.view.FillBackGroundView;
import be.kdg.prog12.background.view.FillImageBackGroundView;
import javafx.application.Application;
import javafx.scene.*;
import javafx.stage.Stage;


public class MainFillImage extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		FillImageBackGroundView view = new FillImageBackGroundView();
		primaryStage.setScene(new Scene(view));
		primaryStage.show();
	}
}