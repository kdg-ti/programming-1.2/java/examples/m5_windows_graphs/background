package be.kdg.prog12.background;

import be.kdg.prog12.background.view.BackGroundImage;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainBackgroundImage extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		BackGroundImage view = new BackGroundImage();
		primaryStage.setScene(new Scene(view));
		primaryStage.show();
	}
}