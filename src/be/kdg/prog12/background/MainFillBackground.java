package be.kdg.prog12.background;

import be.kdg.prog12.background.view.FillBackGroundView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainFillBackground extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		FillBackGroundView view = new FillBackGroundView();
		primaryStage.setScene(new Scene(view));
		primaryStage.show();
	}
}