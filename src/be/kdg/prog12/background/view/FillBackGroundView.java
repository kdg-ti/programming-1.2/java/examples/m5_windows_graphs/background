package be.kdg.prog12.background.view;

import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class FillBackGroundView extends BorderPane {
	private MenuItem miAbout;
	private MenuBar menuBar;


  private Canvas emptyCanvas;
	private HBox hbox;

	public FillBackGroundView() {
		initialiseNodes();
		layoutNodes();
	}

	private void layoutNodes() {
		setTop(menuBar);
		setCenter(hbox);	}

	private void initialiseNodes() {
		miAbout = new MenuItem("About");
		Menu mnHelp = new Menu("Help");
		mnHelp.getItems().addAll(miAbout);
		menuBar = new MenuBar(mnHelp);
		emptyCanvas = new Canvas(480,360);
		hbox = new HBox(emptyCanvas);
		hbox.setBackground(new Background(
			new BackgroundFill(
				Color.rgb(30, 230, 80, 0.9),
				new CornerRadii(90),
				new Insets(10)
			)));
	}
}
