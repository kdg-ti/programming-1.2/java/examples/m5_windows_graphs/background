package be.kdg.prog12.background.view;

import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;

public class FillImageBackGroundView extends BorderPane {

	private MenuItem miAbout;
	private MenuBar menuBar;

	private Canvas emptyCanvas;

	public FillImageBackGroundView() {
		initialiseNodes();
		layoutNodes();
	}

	private void layoutNodes() {
		setTop(menuBar);
		setCenter(emptyCanvas);

	}

	private void initialiseNodes() {
		miAbout = new MenuItem("About");
		Menu mnHelp = new Menu("Help");
		mnHelp.getItems().addAll(miAbout);
		menuBar = new MenuBar(mnHelp);;
		setBackground(new Background(new BackgroundFill(
				new ImagePattern(new Image("/images/goosegame.jpg")),
				new CornerRadii(0),
				new Insets(0)
			)));
		emptyCanvas = new Canvas(480,360);
	}
}
