package be.kdg.prog12.background.view;

import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;

public class BackGroundImage extends BorderPane {

	private MenuItem miAbout;
	private MenuBar menuBar;

	private HBox pane;

	public BackGroundImage() {
		initialiseNodes();
		layoutNodes();
}

	private void layoutNodes() {
		setTop(menuBar);
		setCenter(pane);

	}

	private void initialiseNodes() {
		miAbout = new MenuItem("About");
		Menu mnHelp = new Menu("Help");
		mnHelp.getItems().addAll(miAbout);
		menuBar = new MenuBar(mnHelp);
		Image background = new Image("/images/goosegame.jpg");
		pane = new HBox();
		pane.setBackground(new Background(new BackgroundImage(
				background,
				BackgroundRepeat.NO_REPEAT,
				BackgroundRepeat.NO_REPEAT,
				BackgroundPosition.DEFAULT,
				new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, false)
		)));
		pane.setPrefWidth(background.getWidth());
		pane.setPrefHeight(background.getHeight());
	}
}
